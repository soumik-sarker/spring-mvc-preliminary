package net.therap.coursetraineemanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author soumik.sarker
 * @since 9/26/21
 */
@Controller
@RequestMapping(value="/")
public class Main {

    @RequestMapping(method= RequestMethod.GET)
    public ModelAndView welcome(){
        return new ModelAndView("index", "message", "Welcome to Spring MVC World!!!");
    }
}
